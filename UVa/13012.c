/* 16601941  13012  Identifying tea  ANSI C  0.000  2015-12-18 13:44:03 */

/*
Input: correct answer & each person's answer
Output: how many people answered correctly?
*/

/* Just counting... */

#include <stdio.h>

int main(){
	int correct;
	while( scanf("%d", &correct) != EOF ){
		int i, answer, output = 0;
		for(i = 0; i < 5; ++i){
			scanf("%d", &answer);
			if( answer == correct ) output++;
		}
		printf("%d\n", output);
	}

	return 0;
}