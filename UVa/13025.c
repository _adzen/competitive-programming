/* 16602470  13025  Back to the Past  ANSI C  0.003  2015-12-18 15:37:10 */

/* Input: (none)
Output: the day of week of 29/05/2013 */

/* Open your calendar and print the answer... */

#include <stdio.h>

int main(){
	printf("May 29, 2013 Wednesday\n");
	return 0;
}