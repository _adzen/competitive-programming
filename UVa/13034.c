/* 16602497  13034  Solve Everything :-)  ANSI C  0.000  2015-12-18 15:42:56 */

/* Input: numbers of team will solve the problem
Output: Can all team solve at leat one problem? */

/* Find 0...and cancel the flag */

#include <stdio.h>

int main(){
	int test, ti;
	scanf("%d", &test);

	for(ti = 1; ti <= test; ti++){
		int in, i, solved = 1;
		for(i = 0; i < 13; ++i){
			scanf("%d", &in);
			if(in == 0) solved = 0;
		}
		printf("Set #%d: %s\n", ti, solved ? "Yes" : "No");
	}

	return 0;
}