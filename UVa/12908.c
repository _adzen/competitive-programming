/* 16602560  12908  The book thief  ANSI C  0.073  2015-12-18 16:01:38 */

/*
X = 1+2+3+...+N, but one term is missed.
Try to find N and the missing term.

Input: the sum X
Output: N, the missing term
*/

/*
The missing term always in range [1, N].
Guess the N by X, then check the missing one is in range.
*/

#include <stdio.h>
#include <math.h>

typedef unsigned long long int ull;

int main(){
	ull in;
	while( scanf("%llu", &in) != EOF && in > 0 ){
		ull guess = sqrt( (in+1)*2 );
		ull total = (guess * (guess+1)) / 2;

		while(1){
			if(total > in && total-in <= guess){
				printf("%llu %llu\n", total-in, guess);
				break;
			}

			guess++;
			total += guess;
		}
	}

	return 0;
}