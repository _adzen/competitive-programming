/* 14856762  602B  GNU C  31 ms  396 KB  2015-12-18 08:17:33 */

/* Input: N numbers, with n[i+1]-n[i] <= 1 */
/* Output: Find longest range with (max - min) <= 1 */ 

/*
Sol:

1D DP with two pointers.

while(1){
	First, move the right pointer to extend the range until break the requirement.
	One possible longest range was found.
	Second, move the left pointer to shorten the range until meet the requirement.
}
*/ 

/*
Tutorial:

1. ..., +1, -1, +1, -1, ...
2. Memory: O(N), but it can be implemented in O(1).
3. Bonus: the maximum difference permitted in an almost constant range
          is an arbitrary D.
*/

#include <stdio.h>

int max(int a, int b){
	if(a > b) return a;
	else return b;
}

int main(){
	/* input */
	int n, ns[100005], i;
	scanf("%d", &n);
	for(i = 0; i < n; ++i) scanf("%d", &ns[i]);

	/* answer and two pointers */	
	int maxlen = 1, a = 0, b = 1;   
	/* keep the statistics of the range */
	int rangemin = ns[0], rangemax = ns[0], mincount = 1, maxcount = 1;  

	/* 1D DP with two pointers */
	while(b < n){
		/* First, extend the range until break the requirement */
		while(b < n){
			if(rangemax == rangemin){
				if(ns[b] > rangemax){
					rangemax = ns[b];
					maxcount = 1;
					maxlen = max(maxlen, b-a+1);
				}else if(ns[b] < rangemin){
					rangemin = ns[b];
					mincount = 1;
					maxlen = max(maxlen, b-a+1);
				}else{
					mincount++;
					maxcount++;
					maxlen = max(maxlen, b-a+1);
				}
			}else{
				if(ns[b] == rangemax){
					maxcount++;
					maxlen = max(maxlen, b-a+1);
				}else if(ns[b] == rangemin){
					mincount++;
					maxlen = max(maxlen, b-a+1);
				}else{
					break;     /* break the requirement */
				}
			}
			b++;
		}

		/* Second, shorten the range until meet the requirement */
		if(ns[b] > rangemax){
			while(a < n && mincount > 0){
				if(ns[a] == rangemin) mincount--;
				else maxcount--;
				a++;
			}
			rangemin = rangemax;
			mincount = maxcount;
		}else{    /* ns[b] < rangemin */
			while(a < n && maxcount > 0){
				if(ns[a] == rangemax) maxcount--;
				else mincount--;
				a++;
			}
			rangemax = rangemin;
			maxcount = mincount;
		}
	}

	/* output */
	printf("%d\n", maxlen);

	return 0;
}