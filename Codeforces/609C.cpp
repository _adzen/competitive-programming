// 14885249  609C  GNU C++  46 ms  200 KB  2015-12-19 19:14:05

// Find minimum # of task-moving to balance the loads
// Input: the load of each machine
// Output: minimum # of reassignment

/* 
Sol:
2 kinds of goal states
(1) everyone reach avg.
    e.g.: 1 2 3 4 5 -> 3 3 3 3 3
(2) some reach floor(avg.), some reach ceil(avg.)
    e.g.: 1 2 3 4 5 6 -> 3 3 3 4 4 4

1. calculate the average.
2. if total is dividable with avg.
      -> case (1)
   else
      -> case (2)
3. Sort the loads in ascent order
4. Compute the sum of these difference, then divide by 2
*/

#include <cstdio>
#include <cctype>
#include <cmath>
#include <cstdlib>

#include <algorithm>

using namespace std;

typedef long long int ll;

int main(){
	int n;
	scanf("%d", &n);

	int i, l[100002];
	ll total = 0;    // total of these loads
	for(i = 0; i < n; ++i){
		scanf("%d", &l[i]);
		total += l[i];
	}
	sort(l, l+n);    // sort in ascent order

	if(total % n == 0){    // case (1)
		int mid = total / n;
		ll ans = 0;
		for(i = 0; i < n; ++i) ans += labs(mid-l[i]);
		printf("%I64d\n", ans / 2);
	}else{     // case (2)
		int mid = total / n;
		int nextmid = mid+1;

		// calculate # of floor(avg.) and # of ceil(avg.) 
		// mid * midn + (mid+1) * (n-midn) = total
		int midn = (ll)n * mid + n - total;
		int nextn = (total - (ll)mid * midn) / nextmid;

		// Compute the sum of these difference
		ll ans = 0;
		for(i = 0; i < midn; ++i) ans += labs(mid-l[i]);
		for(i = midn; i < n; ++i) ans += labs(nextmid-l[i]);
		printf("%I64d\n", ans / 2);
	}

	return 0;
}