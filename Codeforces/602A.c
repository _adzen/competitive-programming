/* 14856233  602A  GNU C  15 ms  8 KB  2015-12-18 06:16:16 */

/* Input: X, Y with different bases */
/* Output: compare the value of these two numbers */

#include <stdio.h>

int main(){
	/* input */
	int xlen, xbase, i, x[15];
	scanf("%d%d", &xlen, &xbase);
	for(i = 0; i < xlen; ++i) scanf("%d", &x[i]);

	int ylen, ybase, y[15];
	scanf("%d%d", &ylen, &ybase);
	for(i = 0; i < ylen; ++i) scanf("%d", &y[i]);
	
	/* Warning: 64-bit integer is needed!! */
	unsigned long long xvalue = 0, yvalue = 0, base;
	for(i = xlen-1, base = 1; i >= 0; --i){
		xvalue += base * x[i];
		base *= xbase;
	}
	for(i = ylen-1, base = 1; i >= 0; --i){
		yvalue += base * y[i];
		base *= ybase;
	}

	/* output */
	if(xvalue < yvalue) puts("<");
	else if(xvalue > yvalue) puts(">");
	else puts("=");

	return 0;
}