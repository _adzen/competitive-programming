// 14950319  608B  GNU C++  15 ms  1772 KB  2015-12-23 20:28:11

// Calculate the XOR sum of two binary strings

// Input: two strings
// Output: the XOR sum

/*
Sol:

A = 01
B = 00111

1. calculate the prefix sum of B:
   -> 00123
2. for each a[i], sum up the corresponding # of 0 or 1 in B
   -> 0: 2
   -> 1: 1

Time Complexity: O(|a| + |b|)
Memory Complexity: O(|b|)
*/

#include <cstdio>
#include <cstring>

typedef long long ll;

char a[200002], b[200002];
int b0[200002], b1[200002];

int main(){
	int n, t, i, j, k, o, p;
	scanf("%s%s", a, b);
	
	if(b[0] == '0') b0[1] = 1;
	else b1[1] = 1;
	
	// count the prefix sum of string b
	for(i = 1; b[i]; ++i){
		b0[i+1] = b0[i];
		b1[i+1] = b1[i];
		
		if(b[i] == '0') b0[i+1]++;
		else b1[i+1]++;
	}
	int blen = i;
	//printf("Blen = %d\n", blen);
	
	// for each a[i], calculate the number of b0 of b1
	ll ans = 0;
	int alen = strlen(a);
	for(i = 0; i < alen; ++i){
		if(a[i] == '0'){
			ans += b1[blen-alen+i+1] - b1[i];
		}else{
			ans += b0[blen-alen+i+1] - b0[i];
		}
	}

	printf("%lld\n", ans);

	return 0;
}