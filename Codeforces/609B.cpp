// 14883432  609B  GNU C++  46 ms  0 KB  2015-12-19 18:52:05

// each books has it's gene, how many different ways to buy 2 books with different gene
// Input: each books' gene
// Output: # of different way to buy 2 books

// Sol:
// calculate the statistic of the genes
// answer = sum[ (# of books of gi) * (# of books of gj)] with i < j

#include <cstdio>
#include <cctype>
#include <cmath>
#include <cstdlib>

#include <algorithm>

using namespace std;

typedef long long int ll;

int main(){
	int n, m;
	scanf("%d%d", &n, &m);

	// calculate with frequencies
	int i, fre[10] = {0};    // WA: forget to initialize
	for(i = 0; i < n; ++i){
		int in;
		scanf("%d", &in);
		fre[in-1]++;
	}

	// summation with i < j
	ll ans = 0;
	int j;
	for(i = 0; i < m-1; ++i){
	for(j = i+1; j < m; ++j){
		ans += (ll)fre[i] * (ll)fre[j];
	}}

	printf("%I64d\n", ans);

	return 0;
}