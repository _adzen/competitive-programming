// 14878075  609A  GNU C++  15 ms  0 KB  2015-12-19 18:06:39

// A big file can be splited, find minimum # of USB to store the file
// Input: size of the file, the capacity of the USB's
// Output: the minimum # of USB

// Sol:
// Sort the capacities with desc
// Greedy fill the USBs 

#include <cstdio>
#include <cctype>
#include <cmath>
#include <cstdlib>

#include <algorithm>

using namespace std;

int main(){
	int n;
	scanf("%d", &n);

	int t;
	scanf("%d", &t);

	int list[102], i;
	for(i = 0; i < n; ++i) scanf("%d", &list[i]);
	sort(list, list+n);   // sort the capacities
	
	// Greedy: from Bigest to Smallest
	int ans = 0;
	i = n-1;
	while(t > 0){
		ans++;
		t -= list[i];
		i--;
	}

	printf("%d\n", ans);

	return 0;
}