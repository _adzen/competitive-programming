/* 1883372  5876  Writings on the Wall  ANSI C  1.146  2015-12-18 09:56:09 */

/* Input: two pictures, represented in alphabets */
/* Output: how many ways to overlap two pictures */

/*
Sol:

Precompute the sum of alphabets.
Directly compute the range sum in O(1) time to prun the search.

The Top 20 solutions are below 0.04 sec.
Further improvement is needed.
*/

#include <stdio.h>
#include <string.h>

int min(int a, int b){
	if(a < b) return a;
	else return b;
}

int main(){
	int n;
	scanf("%d", &n);

	while(n--){
		char a[50002], b[50002];
		scanf("%s%s", a, b);
		int suma[50002] = {a[0]}, sumb[50002] = {b[0]};   
		
		/* compute the range sum */
		int lena, lenb;
		for(lena = 1; a[lena]; lena++) suma[lena] = suma[lena-1] + a[lena];
		for(lenb = 1; b[lenb]; lenb++) sumb[lenb] = sumb[lenb-1] + b[lenb];

		int answer = 1, lapLen, maxlap = min(lena, lenb);
		for(lapLen = 1; lapLen <= maxlap; lapLen++){
			/* compute the two subrange sum in O(1) time to prun */
			int left = suma[lena-1] - suma[lena-1-lapLen];
			int right = sumb[lapLen-1];

			if(left == right && strncmp(a+lena-lapLen, b, lapLen) == 0) answer++;
		}

		printf("%d\n", answer);
	}

	return 0;
}