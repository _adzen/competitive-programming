/* 1883369  5870  Smooth Visualization  ANSI C  0.000  2015-12-18 09:20:19 */

/* Input: data sequence (ranged 1~7) */
/* Output: the smoothed historgram (1726 -> 1234567654323456) */

/* just padding (+1 or -1), and draw the output */

#include <stdio.h>
#include <stdlib.h>

int main(){
	int n;
	scanf("%d", &n);

	char in[200];
	while(n-- > 0 && scanf("%s", in) ){
		char out[1000] = {0};   /* output sequence */
		int outi = 0, ini, max = in[0];
		out[0] = in[0];
		
		for(ini = 1; in[ini]; ini++){
			/* check padding is needed or not? */
			while( labs(in[ini]-out[outi]) > 1 ){
				out[outi+1] = (in[ini] > out[outi]) ? (out[outi] + 1) : (out[outi] - 1);
				if(out[outi+1] > max) max = out[outi+1];
				outi++;
			}

			/* append the next digit */
			out[outi+1] = in[ini];
			if(out[outi+1] > max) max = out[outi+1];
			outi++;
		}
		outi++;
		/*printf("max = %c, output = %s\n", max, out);*/
		
		/* draw the output */
		int i, j;
		for(i = max; i >= '1'; --i){
			for(j = 0; j < outi; ++j) putchar( (i <= out[j]) ? '+' : '*' );
			putchar('\n');
		}
	}

	return 0;
}